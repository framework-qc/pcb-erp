package com.ruoyi.erp.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 生产型号对象 erp_model
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpModel extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 产品分类 */
    @Excel(name = "产品分类")
    private String modeType;

    /** 生产pcb文件 */
    @Excel(name = "生产pcb文件")
    private String pcbFile;

    /** 生产机器文件 */
    @Excel(name = "生产机器文件")
    private String machineFile;

    /** 长度 */
    @Excel(name = "长度")
    private BigDecimal length;

    /** 宽度 */
    @Excel(name = "宽度")
    private BigDecimal wide;

    /** 版厚 */
    @Excel(name = "版厚")
    private BigDecimal thickness;

    /** 层数 */
    @Excel(name = "层数")
    private Long layerNum;

    /** 基材 */
    @Excel(name = "基材")
    private String baseMaterial;

    /** 外层铜厚 */
    @Excel(name = "外层铜厚")
    private BigDecimal outerThickness;

    /** 内层铜厚 */
    @Excel(name = "内层铜厚")
    private BigDecimal innerThickness;

    /** 焊盘喷镀 */
    @Excel(name = "焊盘喷镀")
    private String solderCoating;

    /** 阻焊颜色 */
    @Excel(name = "阻焊颜色")
    private String solderColor;

    /** 字符颜色 */
    @Excel(name = "字符颜色")
    private String characterColor;

    /** 成形 */
    @Excel(name = "成形")
    private String shapingStyle;

    /** 测试 */
    @Excel(name = "测试")
    private String testStyle;

    /** 过孔 */
    @Excel(name = "过孔")
    private String viaHole;

    /** 规格 */
    @Excel(name = "规格")
    private String spec;

    /** 单元尺寸 */
    @Excel(name = "单元尺寸")
    private String unitSize;

    /** 拼版尺寸 */
    @Excel(name = "拼版尺寸")
    private String makeUpSize;

    /** 拼板款数 */
    private Long makeModeNum;

    /** 是否阻抗 */
    private String isImpedance;

    /** 阻焊覆盖 */
    private String solderCover;

    /** 最小孔径 */
    private BigDecimal minAperture;

    /** 外径 */
    private BigDecimal outerDiameter;

    /** 线路测试 */
    private String lineTest;

    /** 否需要斜边 */
    private String hypotenuse;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    /** 生产型号名称 */
    @Excel(name = "生产型号名称")
    private String modeName;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setModeType(String modeType) 
    {
        this.modeType = modeType;
    }

    public String getModeType() 
    {
        return modeType;
    }
    public void setPcbFile(String pcbFile) 
    {
        this.pcbFile = pcbFile;
    }

    public String getPcbFile() 
    {
        return pcbFile;
    }
    public void setMachineFile(String machineFile) 
    {
        this.machineFile = machineFile;
    }

    public String getMachineFile() 
    {
        return machineFile;
    }
    public void setLength(BigDecimal length) 
    {
        this.length = length;
    }

    public BigDecimal getLength() 
    {
        return length;
    }
    public void setWide(BigDecimal wide) 
    {
        this.wide = wide;
    }

    public BigDecimal getWide() 
    {
        return wide;
    }
    public void setThickness(BigDecimal thickness) 
    {
        this.thickness = thickness;
    }

    public BigDecimal getThickness() 
    {
        return thickness;
    }
    public void setLayerNum(Long layerNum) 
    {
        this.layerNum = layerNum;
    }

    public Long getLayerNum() 
    {
        return layerNum;
    }
    public void setBaseMaterial(String baseMaterial) 
    {
        this.baseMaterial = baseMaterial;
    }

    public String getBaseMaterial() 
    {
        return baseMaterial;
    }
    public void setOuterThickness(BigDecimal outerThickness) 
    {
        this.outerThickness = outerThickness;
    }

    public BigDecimal getOuterThickness() 
    {
        return outerThickness;
    }
    public void setInnerThickness(BigDecimal innerThickness) 
    {
        this.innerThickness = innerThickness;
    }

    public String getModeName() {
        return modeName;
    }

    public void setModeName(String modeName) {
        this.modeName = modeName;
    }

    public BigDecimal getInnerThickness()
    {
        return innerThickness;
    }
    public void setSolderCoating(String solderCoating) 
    {
        this.solderCoating = solderCoating;
    }

    public String getSolderCoating() 
    {
        return solderCoating;
    }
    public void setSolderColor(String solderColor) 
    {
        this.solderColor = solderColor;
    }

    public String getSolderColor() 
    {
        return solderColor;
    }
    public void setCharacterColor(String characterColor) 
    {
        this.characterColor = characterColor;
    }

    public String getCharacterColor() 
    {
        return characterColor;
    }
    public void setShapingStyle(String shapingStyle) 
    {
        this.shapingStyle = shapingStyle;
    }

    public String getShapingStyle() 
    {
        return shapingStyle;
    }
    public void setTestStyle(String testStyle) 
    {
        this.testStyle = testStyle;
    }

    public String getTestStyle() 
    {
        return testStyle;
    }
    public void setViaHole(String viaHole) 
    {
        this.viaHole = viaHole;
    }

    public String getViaHole() 
    {
        return viaHole;
    }
    public void setSpec(String spec) 
    {
        this.spec = spec;
    }

    public String getSpec() 
    {
        return spec;
    }
    public void setUnitSize(String unitSize) 
    {
        this.unitSize = unitSize;
    }

    public String getUnitSize() 
    {
        return unitSize;
    }
    public void setMakeUpSize(String makeUpSize) 
    {
        this.makeUpSize = makeUpSize;
    }

    public String getMakeUpSize() 
    {
        return makeUpSize;
    }
    public void setMakeModeNum(Long makeModeNum) 
    {
        this.makeModeNum = makeModeNum;
    }

    public Long getMakeModeNum() 
    {
        return makeModeNum;
    }
    public void setIsImpedance(String isImpedance) 
    {
        this.isImpedance = isImpedance;
    }

    public String getIsImpedance() 
    {
        return isImpedance;
    }
    public void setSolderCover(String solderCover) 
    {
        this.solderCover = solderCover;
    }

    public String getSolderCover() 
    {
        return solderCover;
    }
    public void setMinAperture(BigDecimal minAperture) 
    {
        this.minAperture = minAperture;
    }

    public BigDecimal getMinAperture() 
    {
        return minAperture;
    }
    public void setOuterDiameter(BigDecimal outerDiameter) 
    {
        this.outerDiameter = outerDiameter;
    }

    public BigDecimal getOuterDiameter() 
    {
        return outerDiameter;
    }
    public void setLineTest(String lineTest) 
    {
        this.lineTest = lineTest;
    }

    public String getLineTest() 
    {
        return lineTest;
    }
    public void setHypotenuse(String hypotenuse) 
    {
        this.hypotenuse = hypotenuse;
    }

    public String getHypotenuse() 
    {
        return hypotenuse;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("modeNo", getModeNo())
            .append("modeType", getModeType())
            .append("pcbFile", getPcbFile())
            .append("machineFile", getMachineFile())
            .append("length", getLength())
            .append("wide", getWide())
            .append("thickness", getThickness())
            .append("layerNum", getLayerNum())
            .append("baseMaterial", getBaseMaterial())
            .append("outerThickness", getOuterThickness())
            .append("innerThickness", getInnerThickness())
            .append("solderCoating", getSolderCoating())
            .append("solderColor", getSolderColor())
            .append("characterColor", getCharacterColor())
            .append("shapingStyle", getShapingStyle())
            .append("testStyle", getTestStyle())
            .append("viaHole", getViaHole())
            .append("spec", getSpec())
            .append("unitSize", getUnitSize())
            .append("makeUpSize", getMakeUpSize())
            .append("makeModeNum", getMakeModeNum())
            .append("isImpedance", getIsImpedance())
            .append("solderCover", getSolderCover())
            .append("minAperture", getMinAperture())
            .append("outerDiameter", getOuterDiameter())
            .append("lineTest", getLineTest())
            .append("hypotenuse", getHypotenuse())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
