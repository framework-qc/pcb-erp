package com.ruoyi.erp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 出库日志对象 erp_stock_out_log
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpStockOutLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 出库数量 */
    @Excel(name = "出库数量")
    private Long outNum;

    /** 出库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "出库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date outTime;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String customerNo;

    /** 配送方式 */
    @Excel(name = "配送方式")
    private String deliveryWay;

    /** 物流公司 */
    @Excel(name = "物流公司")
    private String logisticsCompany;

    /** 物流公司编码 */
    @Excel(name = "物流公司编码")
    private String logisticsCompanyCode;

    /** 物流编号 */
    @Excel(name = "物流编号")
    private String logisticsNo;

    /** 收货ID */
    @Excel(name = "收货ID")
    private String consigneeId;

    /** 操作人 */
    @Excel(name = "操作人")
    private String optUser;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setBatchNo(String batchNo) 
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo() 
    {
        return batchNo;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setOutNum(Long outNum) 
    {
        this.outNum = outNum;
    }

    public Long getOutNum() 
    {
        return outNum;
    }
    public void setOutTime(Date outTime) 
    {
        this.outTime = outTime;
    }

    public Date getOutTime() 
    {
        return outTime;
    }
    public void setCustomerNo(String customerNo) 
    {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() 
    {
        return customerNo;
    }
    public void setDeliveryWay(String deliveryWay) 
    {
        this.deliveryWay = deliveryWay;
    }

    public String getDeliveryWay() 
    {
        return deliveryWay;
    }
    public void setLogisticsCompany(String logisticsCompany) 
    {
        this.logisticsCompany = logisticsCompany;
    }

    public String getLogisticsCompany() 
    {
        return logisticsCompany;
    }
    public void setLogisticsCompanyCode(String logisticsCompanyCode) 
    {
        this.logisticsCompanyCode = logisticsCompanyCode;
    }

    public String getLogisticsCompanyCode() 
    {
        return logisticsCompanyCode;
    }
    public void setLogisticsNo(String logisticsNo) 
    {
        this.logisticsNo = logisticsNo;
    }

    public String getLogisticsNo() 
    {
        return logisticsNo;
    }
    public void setConsigneeId(String consigneeId) 
    {
        this.consigneeId = consigneeId;
    }

    public String getConsigneeId() 
    {
        return consigneeId;
    }
    public void setOptUser(String optUser) 
    {
        this.optUser = optUser;
    }

    public String getOptUser() 
    {
        return optUser;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("batchNo", getBatchNo())
            .append("orderNo", getOrderNo())
            .append("modeNo", getModeNo())
            .append("outNum", getOutNum())
            .append("outTime", getOutTime())
            .append("customerNo", getCustomerNo())
            .append("deliveryWay", getDeliveryWay())
            .append("logisticsCompany", getLogisticsCompany())
            .append("logisticsCompanyCode", getLogisticsCompanyCode())
            .append("logisticsNo", getLogisticsNo())
            .append("consigneeId", getConsigneeId())
            .append("optUser", getOptUser())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
