package com.ruoyi.erp.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 售后反馈对象 erp_after_feedback
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpAfterFeedback extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 来源渠道 */
    @Excel(name = "来源渠道")
    private String channel;

    /** 客户编号 */
    @Excel(name = "客户编号")
    private String customerNo;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 反馈问题 */
    @Excel(name = "反馈问题")
    private String issueText;

    /** 联系人 */
    @Excel(name = "联系人")
    private String contacts;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contactsPhone;

    /** 联系地址 */
    @Excel(name = "联系地址")
    private String contactsAddress;

    /** 售后状态 */
    @Excel(name = "售后状态")
    private String state;

    /** 操作人 */
    @Excel(name = "操作人")
    private String optUser;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setChannel(String channel) 
    {
        this.channel = channel;
    }

    public String getChannel() 
    {
        return channel;
    }
    public void setCustomerNo(String customerNo) 
    {
        this.customerNo = customerNo;
    }

    public String getCustomerNo() 
    {
        return customerNo;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setIssueText(String issueText) 
    {
        this.issueText = issueText;
    }

    public String getIssueText() 
    {
        return issueText;
    }
    public void setContacts(String contacts) 
    {
        this.contacts = contacts;
    }

    public String getContacts() 
    {
        return contacts;
    }
    public void setContactsPhone(String contactsPhone) 
    {
        this.contactsPhone = contactsPhone;
    }

    public String getContactsPhone() 
    {
        return contactsPhone;
    }
    public void setContactsAddress(String contactsAddress) 
    {
        this.contactsAddress = contactsAddress;
    }

    public String getContactsAddress() 
    {
        return contactsAddress;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setOptUser(String optUser) 
    {
        this.optUser = optUser;
    }

    public String getOptUser() 
    {
        return optUser;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("channel", getChannel())
            .append("customerNo", getCustomerNo())
            .append("orderNo", getOrderNo())
            .append("modeNo", getModeNo())
            .append("issueText", getIssueText())
            .append("contacts", getContacts())
            .append("contactsPhone", getContactsPhone())
            .append("contactsAddress", getContactsAddress())
            .append("state", getState())
            .append("optUser", getOptUser())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
