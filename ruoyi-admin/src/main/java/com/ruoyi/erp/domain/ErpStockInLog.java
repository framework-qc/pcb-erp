package com.ruoyi.erp.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 入库日志对象 erp_stock_in_log
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public class ErpStockInLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键ID */
    private String id;

    /** 入库类型 */
    @Excel(name = "入库类型")
    private String putType;

    /** 批次号 */
    @Excel(name = "批次号")
    private String batchNo;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNo;

    /** 生产型号 */
    @Excel(name = "生产型号")
    private String modeNo;

    /** 入库数量 */
    @Excel(name = "入库数量")
    private Long inNum;

    /** 入库时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Excel(name = "入库时间", width = 30, dateFormat = "yyyy-MM-dd HH:mm:ss")
    private Date inTime;

    /** 操作人 */
    @Excel(name = "操作人")
    private String optUser;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setPutType(String putType) 
    {
        this.putType = putType;
    }

    public String getPutType() 
    {
        return putType;
    }
    public void setBatchNo(String batchNo) 
    {
        this.batchNo = batchNo;
    }

    public String getBatchNo() 
    {
        return batchNo;
    }
    public void setOrderNo(String orderNo) 
    {
        this.orderNo = orderNo;
    }

    public String getOrderNo() 
    {
        return orderNo;
    }
    public void setModeNo(String modeNo) 
    {
        this.modeNo = modeNo;
    }

    public String getModeNo() 
    {
        return modeNo;
    }
    public void setInNum(Long inNum) 
    {
        this.inNum = inNum;
    }

    public Long getInNum() 
    {
        return inNum;
    }
    public void setInTime(Date inTime) 
    {
        this.inTime = inTime;
    }

    public Date getInTime() 
    {
        return inTime;
    }
    public void setOptUser(String optUser) 
    {
        this.optUser = optUser;
    }

    public String getOptUser() 
    {
        return optUser;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("putType", getPutType())
            .append("batchNo", getBatchNo())
            .append("orderNo", getOrderNo())
            .append("modeNo", getModeNo())
            .append("inNum", getInNum())
            .append("inTime", getInTime())
            .append("optUser", getOptUser())
            .append("delFlag", getDelFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
