package com.ruoyi.erp.service.impl;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.BusinessException;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.security.Md5Utils;
import com.ruoyi.common.utils.uuid.IdUtils;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.service.impl.SysUserServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.erp.mapper.ErpCustomerMapper;
import com.ruoyi.erp.domain.ErpCustomer;
import com.ruoyi.erp.service.IErpCustomerService;
import com.ruoyi.common.core.text.Convert;

/**
 * 客户Service业务层处理
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Service
public class ErpCustomerServiceImpl implements IErpCustomerService
{
    private static final Logger log = LoggerFactory.getLogger(ErpCustomerServiceImpl.class);

    @Autowired
    private ErpCustomerMapper erpCustomerMapper;

    /**
     * 查询客户
     * 
     * @param id 客户ID
     * @return 客户
     */
    @Override
    public ErpCustomer selectErpCustomerById(String id)
    {
        return erpCustomerMapper.selectErpCustomerById(id);
    }

    /**
     * 查询客户列表
     * 
     * @param erpCustomer 客户
     * @return 客户
     */
    @Override
    public List<ErpCustomer> selectErpCustomerList(ErpCustomer erpCustomer)
    {
        return erpCustomerMapper.selectErpCustomerList(erpCustomer);
    }

    /**
     * 新增客户
     * 
     * @param erpCustomer 客户
     * @return 结果
     */
    @Override
    public int insertErpCustomer(ErpCustomer erpCustomer)
    {
        erpCustomer.setId(IdUtils.fastSimpleUUID());
        erpCustomer.setCreateTime(DateUtils.getNowDate());
        return erpCustomerMapper.insertErpCustomer(erpCustomer);
    }

    /**
     * 修改客户
     * 
     * @param erpCustomer 客户
     * @return 结果
     */
    @Override
    public int updateErpCustomer(ErpCustomer erpCustomer)
    {
        erpCustomer.setUpdateTime(DateUtils.getNowDate());
        return erpCustomerMapper.updateErpCustomer(erpCustomer);
    }

    /**
     * 删除客户对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteErpCustomerByIds(String ids)
    {
        return erpCustomerMapper.deleteErpCustomerByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除客户信息
     * 
     * @param id 客户ID
     * @return 结果
     */
    @Override
    public int deleteErpCustomerById(String id)
    {
        return erpCustomerMapper.deleteErpCustomerById(id);
    }

    /**
     * 导入数据
     *
     * @param list            用户数据列表
     * @param isUpdateSupport 是否更新支持，如果已存在，则进行更新数据
     * @param operName        操作用户
     * @return 结果
     */
    @Override
    public String importData(List<ErpCustomer> list, Boolean isUpdateSupport, String operName) {
        if (StringUtils.isNull(list) || list.size() == 0) {
            throw new BusinessException("导入客户数据不能为空！");
        }
        int successNum = 0;
        int failureNum = 0;
        StringBuilder successMsg = new StringBuilder();
        StringBuilder failureMsg = new StringBuilder();
        for (ErpCustomer insertPo : list) {
            try {
                // 验证是否存在这个用户
                ErpCustomer erpCustomer = new ErpCustomer();
                erpCustomer.setCompanyName(insertPo.getCompanyName());
                List<ErpCustomer> u = erpCustomerMapper.selectErpCustomerList(erpCustomer);
                if (null == u || u.size() == 0) {
                    insertPo.setId(IdUtils.fastSimpleUUID());
                    this.insertErpCustomer(insertPo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + insertPo.getCompanyName() + " 导入成功");
                } else if (isUpdateSupport) {
                    this.updateErpCustomer(insertPo);
                    successNum++;
                    successMsg.append("<br/>" + successNum + "、账号 " + insertPo.getCompanyName() + " 更新成功");
                } else {
                    failureNum++;
                    failureMsg.append("<br/>" + failureNum + "、账号 " + insertPo.getCompanyName() + " 已存在");
                }
            } catch (Exception e) {
                failureNum++;
                String msg = "<br/>" + failureNum + "、账号 " + insertPo.getCompanyName() + " 导入失败：";
                failureMsg.append(msg + e.getMessage());
                log.error(msg, e);
            }
        }
        if (failureNum > 0) {
            failureMsg.insert(0, "很抱歉，导入失败！共 " + failureNum + " 条数据格式不正确，错误如下：");
            throw new BusinessException(failureMsg.toString());
        } else {
            successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
        }
        return successMsg.toString();
    }

}
