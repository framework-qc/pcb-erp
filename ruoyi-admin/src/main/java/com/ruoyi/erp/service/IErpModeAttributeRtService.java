package com.ruoyi.erp.service;

import java.util.List;
import com.ruoyi.erp.domain.ErpModeAttributeRt;

/**
 * 生产型号拓展属性关联Service接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface IErpModeAttributeRtService 
{
    /**
     * 查询生产型号拓展属性关联
     * 
     * @param id 生产型号拓展属性关联ID
     * @return 生产型号拓展属性关联
     */
    public ErpModeAttributeRt selectErpModeAttributeRtById(String id);

    /**
     * 查询生产型号拓展属性关联列表
     * 
     * @param erpModeAttributeRt 生产型号拓展属性关联
     * @return 生产型号拓展属性关联集合
     */
    public List<ErpModeAttributeRt> selectErpModeAttributeRtList(ErpModeAttributeRt erpModeAttributeRt);

    /**
     * 新增生产型号拓展属性关联
     * 
     * @param erpModeAttributeRt 生产型号拓展属性关联
     * @return 结果
     */
    public int insertErpModeAttributeRt(ErpModeAttributeRt erpModeAttributeRt);

    /**
     * 修改生产型号拓展属性关联
     * 
     * @param erpModeAttributeRt 生产型号拓展属性关联
     * @return 结果
     */
    public int updateErpModeAttributeRt(ErpModeAttributeRt erpModeAttributeRt);

    /**
     * 批量删除生产型号拓展属性关联
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpModeAttributeRtByIds(String ids);

    /**
     * 删除生产型号拓展属性关联信息
     * 
     * @param id 生产型号拓展属性关联ID
     * @return 结果
     */
    public int deleteErpModeAttributeRtById(String id);
}
