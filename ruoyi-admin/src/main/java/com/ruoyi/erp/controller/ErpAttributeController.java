package com.ruoyi.erp.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpAttribute;
import com.ruoyi.erp.service.IErpAttributeService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 拓展属性Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpAttribute")
public class ErpAttributeController extends BaseController
{
    private String prefix = "erp/erpAttribute";

    @Autowired
    private IErpAttributeService erpAttributeService;

    @RequiresPermissions("erp:erpAttribute:view")
    @GetMapping()
    public String erpAttribute()
    {
        return prefix + "/erpAttribute";
    }

    /**
     * 查询拓展属性列表
     */
    @RequiresPermissions("erp:erpAttribute:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpAttribute erpAttribute)
    {
        startPage();
        List<ErpAttribute> list = erpAttributeService.selectErpAttributeList(erpAttribute);
        return getDataTable(list);
    }

    /**
     * 导出拓展属性列表
     */
    @RequiresPermissions("erp:erpAttribute:export")
    @Log(title = "拓展属性", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpAttribute erpAttribute)
    {
        List<ErpAttribute> list = erpAttributeService.selectErpAttributeList(erpAttribute);
        ExcelUtil<ErpAttribute> util = new ExcelUtil<ErpAttribute>(ErpAttribute.class);
        return util.exportExcel(list, "erpAttribute");
    }

    /**
     * 新增拓展属性
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存拓展属性
     */
    @RequiresPermissions("erp:erpAttribute:add")
    @Log(title = "拓展属性", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpAttribute erpAttribute)
    {
        return toAjax(erpAttributeService.insertErpAttribute(erpAttribute));
    }

    /**
     * 修改拓展属性
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpAttribute erpAttribute = erpAttributeService.selectErpAttributeById(id);
        mmap.put("erpAttribute", erpAttribute);
        return prefix + "/edit";
    }

    /**
     * 修改保存拓展属性
     */
    @RequiresPermissions("erp:erpAttribute:edit")
    @Log(title = "拓展属性", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpAttribute erpAttribute)
    {
        return toAjax(erpAttributeService.updateErpAttribute(erpAttribute));
    }

    /**
     * 删除拓展属性
     */
    @RequiresPermissions("erp:erpAttribute:remove")
    @Log(title = "拓展属性", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpAttributeService.deleteErpAttributeByIds(ids));
    }
}
