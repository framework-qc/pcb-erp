package com.ruoyi.erp.controller;

import java.util.List;

import com.ruoyi.erp.domain.ErpOrder;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.erp.domain.ErpAfterFeedback;
import com.ruoyi.erp.service.IErpAfterFeedbackService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 售后反馈Controller
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
@Controller
@RequestMapping("/erp/erpAfterFeedback")
public class ErpAfterFeedbackController extends BaseController
{
    private String prefix = "erp/erpAfterFeedback";

    @Autowired
    private IErpAfterFeedbackService erpAfterFeedbackService;

    @RequiresPermissions("erp:erpAfterFeedback:view")
    @GetMapping()
    public String erpAfterFeedback()
    {
        return prefix + "/erpAfterFeedback";
    }

    /**
     * 查询售后反馈列表
     */
    @RequiresPermissions("erp:erpAfterFeedback:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(ErpAfterFeedback erpAfterFeedback)
    {
        startPage();
        List<ErpAfterFeedback> list = erpAfterFeedbackService.selectErpAfterFeedbackList(erpAfterFeedback);
        return getDataTable(list);
    }

    /**
     * 导出售后反馈列表
     */
    @RequiresPermissions("erp:erpAfterFeedback:export")
    @Log(title = "售后反馈", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(ErpAfterFeedback erpAfterFeedback)
    {
        List<ErpAfterFeedback> list = erpAfterFeedbackService.selectErpAfterFeedbackList(erpAfterFeedback);
        ExcelUtil<ErpAfterFeedback> util = new ExcelUtil<ErpAfterFeedback>(ErpAfterFeedback.class);
        return util.exportExcel(list, "erpAfterFeedback");
    }

    /**
     * 新增售后反馈
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存售后反馈
     */
    @RequiresPermissions("erp:erpAfterFeedback:add")
    @Log(title = "售后反馈", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(ErpAfterFeedback erpAfterFeedback)
    {
        return toAjax(erpAfterFeedbackService.insertErpAfterFeedback(erpAfterFeedback));
    }

    /**
     * 修改售后反馈
     */
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") String id, ModelMap mmap)
    {
        ErpAfterFeedback erpAfterFeedback = erpAfterFeedbackService.selectErpAfterFeedbackById(id);
        mmap.put("erpAfterFeedback", erpAfterFeedback);
        return prefix + "/edit";
    }

    /**
     * 修改保存售后反馈
     */
    @RequiresPermissions("erp:erpAfterFeedback:edit")
    @Log(title = "售后反馈", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(ErpAfterFeedback erpAfterFeedback)
    {
        return toAjax(erpAfterFeedbackService.updateErpAfterFeedback(erpAfterFeedback));
    }

    /**
     * 删除售后反馈
     */
    @RequiresPermissions("erp:erpAfterFeedback:remove")
    @Log(title = "售后反馈", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(erpAfterFeedbackService.deleteErpAfterFeedbackByIds(ids));
    }

    /**
     * 查看详细
     */
    @GetMapping("/detail/{id}")
    public String detail(@PathVariable("id") String id, ModelMap mmap) {
        ErpAfterFeedback erpAfterFeedback = erpAfterFeedbackService.selectErpAfterFeedbackById(id);
        mmap.put("erpAfterFeedback", erpAfterFeedback);
        return prefix + "/detail";
    }
}
