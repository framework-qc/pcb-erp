package com.ruoyi.erp.mapper;

import java.util.List;
import com.ruoyi.erp.domain.ErpCustomer;

/**
 * 客户Mapper接口
 * 
 * @author 畅聚科技.Ltd
 * @date 2021-02-28
 */
public interface ErpCustomerMapper 
{
    /**
     * 查询客户
     * 
     * @param id 客户ID
     * @return 客户
     */
    public ErpCustomer selectErpCustomerById(String id);

    /**
     * 查询客户列表
     * 
     * @param erpCustomer 客户
     * @return 客户集合
     */
    public List<ErpCustomer> selectErpCustomerList(ErpCustomer erpCustomer);

    /**
     * 新增客户
     * 
     * @param erpCustomer 客户
     * @return 结果
     */
    public int insertErpCustomer(ErpCustomer erpCustomer);

    /**
     * 修改客户
     * 
     * @param erpCustomer 客户
     * @return 结果
     */
    public int updateErpCustomer(ErpCustomer erpCustomer);

    /**
     * 删除客户
     * 
     * @param id 客户ID
     * @return 结果
     */
    public int deleteErpCustomerById(String id);

    /**
     * 批量删除客户
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteErpCustomerByIds(String[] ids);
}
